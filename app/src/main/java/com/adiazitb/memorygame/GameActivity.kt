package com.adiazitb.memorygame

import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.os.Looper
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.adiazitb.memorygame.databinding.GameHardBinding
import com.adiazitb.memorygame.databinding.GameSuperhardBinding


class GameActivity : AppCompatActivity() {

    val cardImages: Array<Int> =
        arrayOf(R.drawable.earth2, R.drawable.fire2, R.drawable.wind2, R.drawable.water2)
    val cardImagesWhite: Array<Int> =
        arrayOf(R.drawable.earth, R.drawable.fire, R.drawable.wind, R.drawable.water)
    val cardBackgrounds: Array<Int> =
        arrayOf(R.drawable.bgblack, R.drawable.bg_green, R.drawable.bg_red)
    var difficulty = ""
    var turn = 1
    var cardsSelected =  mutableListOf(0,0,0)
    var cardValues: MutableList<Int> = mutableListOf(0, 0, 1, 1, 2, 2, 3, 3)
    var superCardValues: MutableList<Int> = mutableListOf(0, 0, 0, 1, 1, 1, 2, 2, 2)
    var maxScore = 0
    var score = 0
    var movements = 0
    var revealed = BooleanArray(9) {false}
    var locked = BooleanArray(9) {false}
    var currentTime = 0
    var isCounting = true
    var survival = true
    var survivalScore = 0
    var startTimeOriginal = 30000L
    var startTimeUpdated = 30000L
    lateinit var music :MediaPlayer
    lateinit var resultIntent: Intent
    lateinit var binding: GameHardBinding
    lateinit var superbinding: GameSuperhardBinding
    lateinit var timer :CountDownTimer



    override fun onSaveInstanceState(outState: Bundle) {
        outState.putInt("turn",turn)
        outState.putIntArray("cardsSelected", cardsSelected.toIntArray())
        outState.putInt("score", score)
        outState.putInt("movements", movements)
        outState.putString("difficulty",difficulty)
        outState.putIntegerArrayList ("cardValues", ArrayList(cardValues))
        outState.putIntegerArrayList("superCardValues", ArrayList(superCardValues))
        outState.putBooleanArray("revealed", revealed)
        outState.putBooleanArray("locked", locked)
        outState.putInt("currentTime", currentTime)
        outState.putInt("survivalScore", survivalScore)
        outState.putLong("startTimeOriginal", startTimeOriginal)
        outState.putLong("startTimeUpdate", startTimeUpdated)
        super.onSaveInstanceState(outState)
        timer.cancel()
        music.stop()
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        difficulty = intent.extras?.getString("difficulty")!!
        survival = intent.extras?.getBoolean("survival")!!
        setTheme(R.style.Theme_MemoryGame)
        val pauseBuilder = AlertDialog.Builder(this)
        pauseBuilder.setTitle(R.string.pauseTitle)
            .setMessage(R.string.pauseMsg)
            .setNegativeButton("Resume"){ dialog, id ->
                SetTimer()
                dialog.cancel()
            }
        val pauseMsg = pauseBuilder.create()



        super.onCreate(savedInstanceState)
        binding = GameHardBinding.inflate(layoutInflater)
        superbinding = GameSuperhardBinding.inflate(layoutInflater)
        if (difficulty == "superhard")
            setContentView(superbinding.root)
        else
            setContentView(binding.root)



        if (difficulty == "superhard")
            superbinding.PauseBtn.setOnClickListener{
                timer.cancel()
                PauseGame(pauseMsg)
            }
        else
        {
            binding.PauseBtn.setOnClickListener{
                timer.cancel()
                PauseGame(pauseMsg)
            }
        }
        val cardLayouts: Array<ImageView> = arrayOf(
            binding.card1image, binding.card2image, binding.card3image, binding.card4image,
            binding.card5image, binding.card6image, binding.card7image, binding.card8image
        )
        val cardButtons: Array<ImageButton> = arrayOf(
            binding.card1button, binding.card2button, binding.card3button, binding.card4button,
            binding.card5button, binding.card6button, binding.card7button, binding.card8button
        )
        val superCardLayouts: Array<ImageView> = arrayOf(
            superbinding.card1image,
            superbinding.card2image,
            superbinding.card3image,
            superbinding.card4image,
            superbinding.card5image,
            superbinding.card6image,
            superbinding.card7image,
            superbinding.card8image,
            superbinding.card9image
        )
        val superCardButtons: Array<ImageButton> = arrayOf(
            superbinding.card1button,
            superbinding.card2button,
            superbinding.card3button,
            superbinding.card4button,
            superbinding.card5button,
            superbinding.card6button,
            superbinding.card7button,
            superbinding.card8button,
            superbinding.card9button
        )
        if (difficulty == "normal") {
            binding.card7.isVisible = false
            binding.card8.isVisible = false
            while (cardValues.count() > 6)
                cardValues.removeLast()

        }
        if (difficulty == "hard") {
            binding.card7.isVisible = true
            binding.card8.isVisible = true
        }
        music = MediaPlayer.create(this, R.raw.adding_the_sun)
        resultIntent = Intent(this, ResultActivity::class.java)
        resultIntent.putExtra("difficulty", difficulty)
        resultIntent.putExtra("survival", survival)
        if (savedInstanceState != null) {

            turn = savedInstanceState.getInt("turn")
            cardsSelected = savedInstanceState.getIntArray("cardsSelected")!!.toMutableList()
            score = savedInstanceState.getInt("score")
            movements = savedInstanceState.getInt("movements")
            difficulty = savedInstanceState.getString("difficulty")!!
            cardValues = savedInstanceState.getIntegerArrayList("cardValues")!!.toMutableList()
            superCardValues = savedInstanceState.getIntegerArrayList("superCardValues")!!.toMutableList()
            revealed = savedInstanceState.getBooleanArray("revealed")!!
            locked = savedInstanceState.getBooleanArray("locked")!!
            currentTime = savedInstanceState.getInt("currentTime")
            startTimeOriginal = savedInstanceState.getLong("startTimeOriginal")
            startTimeUpdated = savedInstanceState.getLong("startTimeUpdate")
            survivalScore = savedInstanceState.getInt("survivalScore")
            binding.MoveNumber.text = movements.toString()
            superbinding.MoveNumber.text = movements.toString()
            isCounting = false
            SetTimer()
            music.start()
        } else {
            cardValues.shuffle()
            superCardValues.shuffle()

            music.start()
            startTimeOriginal = if (survival) 30000L else 999000L
            startTimeUpdated = startTimeOriginal
            SetTimer()
        }






        if (difficulty == "normal" || difficulty == "hard") {
            binding.TestResult.visibility = View.INVISIBLE
            binding.TestResult.setOnClickListener {
                music.stop()
                startActivity(resultIntent)
            }
        }
        maxScore = if (difficulty == "hard") 4 else 3
        var assignCardIndex = 0

        when (difficulty) {
            "normal", "hard" -> {
                for (card in cardValues) {
                    cardLayouts[assignCardIndex].setImageResource(
                        if (locked[assignCardIndex]) cardImages[cardValues[assignCardIndex]] else cardImagesWhite[cardValues[assignCardIndex]])
                    cardLayouts[assignCardIndex].visibility =
                        if (revealed[assignCardIndex]) View.VISIBLE else View.INVISIBLE
                    assignCardIndex++
                }
                for (i: Int in 0..7)

                    cardButtons[i].setOnClickListener {
                        println("CLICK")
                        cardLayouts[i].visibility = View.VISIBLE
                        if (turn == 1) {
                            turn = 2
                            cardsSelected[0] = i
                        } else if (turn == 2) {
                            cardsSelected[1] = i
                            turn = 1
                            if (CheckPairs(
                                    cardsSelected,
                                    cardLayouts,
                                    cardButtons,
                              )
                            ) {
                                music.stop()
                                startActivity(resultIntent)
                            }
                        }

                        cardsSelected[turn - 1] = i
                        cardButtons[i].isClickable = false
                        revealed[i] = true
                    }
                    }
            "superhard" -> {
                for (card in superCardValues) {
                    superCardLayouts[assignCardIndex].setImageResource(
                        if (locked[assignCardIndex]) cardImages[superCardValues[assignCardIndex]] else cardImagesWhite[superCardValues[assignCardIndex]])
                    superCardLayouts[assignCardIndex].visibility =
                        if (revealed[assignCardIndex]) View.VISIBLE else View.INVISIBLE
                    assignCardIndex++
                }
                for (i: Int in 0..8)

                    superCardButtons[i].setOnClickListener {
                        println("CLICK"+turn)
                        superCardLayouts[i].visibility = View.VISIBLE
                        cardsSelected[turn-1] = i
                        superCardButtons[i].isClickable = false
                        revealed[i] = true
                        println(cardsSelected)
                        if (turn < 3)
                                turn ++
                        else {
                            turn = 1
                            println("checking triple")
                            if (CheckTriples(
                                    cardsSelected,
                                    superCardLayouts,
                                    superCardButtons,
                                )
                            ) {
                                music.stop()
                                startActivity(resultIntent)
                            }
                        }
                    }
            }
        }
    }

    private fun PauseGame(pauseMsg :AlertDialog) {

        isCounting = false
        pauseMsg.show()


    }

    fun CheckPairs(
        cardsSelected: MutableList<Int>,
        cardLayouts: Array<ImageView>,
        cardButtons: Array<ImageButton>,
    ): Boolean {
        while (cardsSelected.count() > 2)
            cardsSelected.removeLast()
        var lockedCardsSelected = cardsSelected.toIntArray().copyOf()
        if (cardValues[cardsSelected[0]] != cardValues[cardsSelected[1]]) {
            wrongShow(lockedCardsSelected.toTypedArray(), cardLayouts, cardButtons)
        } else {
            correctShow(lockedCardsSelected.toTypedArray(), cardLayouts, cardButtons)
        }
        return ProcessMovement()
    }

    fun ProcessMovement(): Boolean {
        movements++
            binding.MoveNumber.text = movements.toString()
            superbinding.MoveNumber.text = movements.toString()
        if (score == maxScore) {
            if (!survival)
            {
            resultIntent.putExtra("FinalMoves", movements)
            resultIntent.putExtra("Time", currentTime)
            return true
            }
            else
            {
                SurvivalReset()
                return false
            }
        } else {
            println("movement ended")
            return false
        }
    }

    private fun SurvivalReset() {
        Handler(Looper.getMainLooper()).postDelayed({
            score = 0
            revealed = BooleanArray(9) { false }
            locked = BooleanArray(9) { false }
            cardValues.shuffle()
            superCardValues.shuffle()
            super.recreate()
        }, 500)
    }

    fun wrongShow(
        cardsSelected: Array<Int>,
        cardLayouts: Array<ImageView>,
        cardButtons: Array<ImageButton>
    ) {
        for (i in 0 until cardsSelected.count()) {
            cardButtons[cardsSelected[i]].setImageResource(cardBackgrounds[2])
            Handler(Looper.getMainLooper()).postDelayed({
                // accions a realitzar un cop hagi passat el temps_en_milisegons
                cardLayouts[cardsSelected[i]].visibility = View.INVISIBLE
                cardButtons[cardsSelected[i]].setImageResource(cardBackgrounds[0])
                cardButtons[cardsSelected[i]].isClickable = true
                revealed[cardsSelected[i]] = false
            }, 500)
            println("wrong Ended")
        }
    }


    fun correctShow(
        cardsSelected: Array<Int>,
        cardLayouts: Array<ImageView>,
        cardButtons: Array<ImageButton>
    ) {
        for (i in 0 until cardsSelected.count()) {
            cardButtons[cardsSelected[i]].setImageResource(cardBackgrounds[1])
            locked[cardsSelected[i]] = true
            Handler(Looper.getMainLooper()).postDelayed({
                // accions a realitzar un cop hagi passat el temps_en_milisegons
                cardLayouts[cardsSelected[i]].setImageResource(cardImages[if(difficulty!="superhard")
                    cardValues[cardsSelected[i]] else superCardValues[cardsSelected[i]]])
                cardButtons[cardsSelected[i]].setImageResource(cardBackgrounds[0])
            }, 500)
        }
        score++
        survivalScore++
        println(score)

    }

    fun CheckTriples(
        cardsSelected: MutableList<Int>,
        cardLayouts: Array<ImageView>,
        cardButtons: Array<ImageButton>,
    ): Boolean {

        var lockedCardsSelected = cardsSelected.toIntArray().copyOf()
        if (superCardValues[cardsSelected[0]] == superCardValues[cardsSelected[1]] && superCardValues[cardsSelected[1]] == superCardValues[cardsSelected[2]]) {
            println("Is Correct!")
            correctShow(lockedCardsSelected.toTypedArray(), cardLayouts, cardButtons)
        } else {
            println("Is not Correct...")
            wrongShow(lockedCardsSelected.toTypedArray(), cardLayouts, cardButtons)
        }
        println("processing movemet")
        return ProcessMovement()
    }

    fun SetTimer(){
        timer = object : CountDownTimer(startTimeUpdated, 100) {
            override fun onTick(millisUntilFinished: Long) {
                startTimeUpdated = millisUntilFinished
                currentTime = ((startTimeOriginal-millisUntilFinished)/1000).toInt()
                binding.TimeNumber.text = currentTime.toString()
                superbinding.TimeNumber.text = currentTime.toString()
            }

            override fun onFinish() {
                SurvivalResults()
            }

        }.start()
    }


    fun SurvivalResults(){
        resultIntent.putExtra("survivalScore", survivalScore)
        resultIntent.putExtra("Time", currentTime)
        music.stop()
        startActivity(resultIntent)
    }
}
