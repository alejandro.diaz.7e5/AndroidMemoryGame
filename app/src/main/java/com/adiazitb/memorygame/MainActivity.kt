package com.adiazitb.memorygame


import android.content.Intent
import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.widget.AdapterView
import androidx.appcompat.app.AlertDialog
import com.adiazitb.memorygame.databinding.MenuBinding
import kotlin.concurrent.thread


class MainActivity : AppCompatActivity() {
    //override fun onCreate(savedInstanceState: Bundle?) {
    /*super.onCreate(savedInstanceState)
    setContentView(R.layout.menu)
    val gameIntent = Intent(this, GameActivity::class.java)
    val playBtn:Button = findViewById(R.id.playBtn)*/

    lateinit var binding: MenuBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.SplashTheme)
        super.onCreate(savedInstanceState)
        binding = MenuBinding.inflate(layoutInflater)
        val music = MediaPlayer.create(this, R.raw.a_very_brady_special)
        StartMusic(music)
        setContentView(binding.root)
        val gameIntent = Intent(this, GameActivity::class.java)
        var difficulty = "normal";
        val difficultylist: Array<String> = arrayOf("normal", "hard", "superhard")
        binding.difficulty.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View?, pos: Int, id: Long) {
                // An item was selected. You can retrieve the selected item using
                // parent.getItemAtPosition(pos)
                difficulty = difficultylist[pos]
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // Another interface callback
            }
        }
        binding.playBtn.setOnClickListener {
            gameIntent.putExtra("difficulty", difficulty)
            music.stop()
            gameIntent.putExtra("survival", binding.Survival!!.isChecked)
            startActivity(gameIntent)
        }

        val helpBuilder = AlertDialog.Builder(this)
        helpBuilder.setTitle(R.string.helpTitle)
            .setMessage(R.string.helpMessage)
            .setNegativeButton("OK"){ dialog, id ->
                dialog.cancel()
            }


        val helpMsg = helpBuilder.create()
        binding.helpBtn.setOnClickListener {
            println("help clicked")
            helpMsg.show()
        }
        val creditBuilder = AlertDialog.Builder(this)
        creditBuilder.setTitle(R.string.creditsTitle)
            .setMessage(R.string.creditsMessage)
            .setNegativeButton("OK"){ dialog, id ->
                dialog.cancel()
            }
       val creditMsg = creditBuilder.create()
        binding.creditBtn.setOnClickListener {
            println("credit clicked")
            creditMsg.show()
        }

    }
    fun StartMusic(music: MediaPlayer)
    {
            music.start()
    }
}


