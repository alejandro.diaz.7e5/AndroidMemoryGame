package com.adiazitb.memorygame

import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.adiazitb.memorygame.databinding.ResultBinding

class ResultActivity : AppCompatActivity() {
    lateinit var binding: ResultBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ResultBinding.inflate(layoutInflater)
        if(intent.extras?.getBoolean("survival")!!){
            binding.moveTxt.text = "Survival Score"
            binding.moveNum.text = intent.extras?.getInt("survivalScore").toString()
        } else
            binding.moveNum.text = intent.extras?.getInt("FinalMoves").toString()

        binding.timeNum.text = intent.extras?.getInt("Time").toString()
        setContentView(binding.root)
        val music = MediaPlayer.create(this, R.raw.canon_in_d_for_two_harps)
        music.start()
        val menuIntent = Intent(this, MainActivity::class.java)
        val gameIntent = Intent(this, GameActivity::class.java)
        gameIntent.putExtra("difficulty", intent.extras?.getString("difficulty"))
        gameIntent.putExtra("survival", intent.extras?.getBoolean("survival"))
        binding.MenuBtn.setOnClickListener {
            music.stop()
            startActivity(menuIntent)
        }
        binding.AgainBtn.setOnClickListener {
            music.stop()
            startActivity(gameIntent)
        }
    }
}